﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PatrolEnemies : Enemy
{
    public Transform[] WayPoints;

    private NavMeshAgent m_Agent;

    private int m_IndexArray;

    private Transform m_CurrentWaypoint;


    // Start is called before the first frame update
    public override void Awake()
    {
        base.Awake();
        m_Agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (enemyState == EnemyState.Patrol)
        {
            Patrol();
        }
        else if (enemyState == EnemyState.Alert)
        {
            //cause the game Over
        }
    }

    void Patrol()
    {

    }

    void SetDestination()
    {

    }
}
