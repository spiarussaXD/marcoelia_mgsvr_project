﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public enum EnemyState
    {
        Patrol,
        Alert
    }

    #region public Vars
    [Tooltip("the range of sight of our enemy")]
    public float ViewRadius;

    [Range(0, 360)]
    [Tooltip("the angulation of our cone sight")]
    public float ViewAngle;

    [Tooltip("the layerMask of our target")]
    public LayerMask DetectionMask;

    //the behaviour of the enemy will change in base of this enum
    [HideInInspector]
    public EnemyState enemyState;


    #endregion

    #region Private Vars
    //storing the targets that i found during the detection
    private Collider[] detectedColliders = new Collider[5];

    //the count of targets
    private int numberOfCollisions;

    //in case of i detected the player stores his last known position
    private Vector3 playerPos;

    private bool playerDetected;

    #endregion

    #region Drawing The ConeSight
#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        EditorGizmo(transform);
    }

    public void EditorGizmo(Transform transform)
    {
        ViewGizmo(Color.green, ViewAngle, ViewRadius);
    }

    private void ViewGizmo(Color color, float angle, float radius)
    {
        Color c = color;
        UnityEditor.Handles.color = c;

        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
    }
#endif
    #endregion

    public virtual void Awake()
    {
        enemyState = EnemyState.Patrol;
    }
    // Update is called once per frame
     public virtual void Update()
     {
        DetectPlayer();
     }

    public void DetectPlayer()
    {
        numberOfCollisions = Physics.OverlapSphereNonAlloc(transform.position, ViewRadius, detectedColliders, DetectionMask);
        for(int i = 0; i < numberOfCollisions; i++)
        {
            float angle = Vector3.Angle(transform.forward, detectedColliders[i].transform.position - transform.position);
            if (Mathf.Abs(angle) < ViewAngle / 2)
            {
                //cast a ray that will do the following things: if the raycast starting at my position that goes through the position of the collider is a wall the
                //result will be that the player is not spotted...else is true and cause the gameOver
                playerPos = detectedColliders[i].transform.position;
                playerDetected = true;
                enemyState = EnemyState.Alert;
                //send the feedback
            }
            else
            {
                playerDetected = false;
                enemyState = EnemyState.Patrol;
            }
        }
        if (numberOfCollisions == 0)
        {
            playerDetected = false;
        }
    }

    //when i spot the player i'll set the timescale to 0.5 and enable an object
    public void SendSpottedFeedBack()
    {

    }



}
