﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Player : MonoBehaviour
{

    public float WalkingSpeed;
    public float RunningSpeed;
    public float MovementSpeed
    {
        get
        {
            bool isRunning = Input.GetButton("Run");

            return isRunning ?
                RunningSpeed :
                WalkingSpeed;
        }
    }
    public float AttackDelay;
    public float MeleeRange;
    [Range(0,360)]
    public float MeleeAngle;

    private float m_LastTimeAttack;
    private NavMeshAgent m_Agent;

    // Start is called before the first frame update
    void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    void Movement()
    {
        Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        transform.position += MovementSpeed * direction * Time.deltaTime;

        if (direction != Vector3.zero)
        {
            transform.forward = direction;
        }
    }

    void Attack()
    {
        if(Time.time> m_LastTimeAttack + AttackDelay && Input.GetButtonDown("Fire1"))
        {
            m_LastTimeAttack = Time.time;
            Collider[] colliders = Physics.OverlapSphere(transform.position, MeleeRange);
            foreach(Collider collider in colliders)
            {
                Vector3 direction = collider.transform.position - transform.position;
                float angle = Vector3.Angle(transform.forward, direction);
                if (Mathf.Abs(angle) < MeleeAngle / 2)
                {
                    //apply the stun for the enemy;
                }
            }
        }
    }

    #region Drawing the Attack cone sight
#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        EditorGizmo(transform);
    }

    public void EditorGizmo(Transform transform)
    {
        ViewGizmo(Color.red, MeleeAngle, MeleeRange);
    }

    private void ViewGizmo(Color color, float angle, float radius)
    {
        Color c = color;
        UnityEditor.Handles.color = c;

        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
    }
#endif
    #endregion
}
